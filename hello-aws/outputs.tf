output "aws_instance_eips" {
  value = aws_eip.instance_eip[*].public_ip
}

locals {
  curl_cmds = [
    for i in aws_eip.instance_eip[*].public_ip:
      format("curl http://%s", i)
  ]
}
output "curl" {
  value = join("\n", local.curl_cmds)
}
