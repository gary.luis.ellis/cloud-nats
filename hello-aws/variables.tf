variable "name" {
  type = string
}

variable "instance_count" {
  type = number
  default = 1
}

variable "instance_ami_name" {
  type = string
  default = "CentOS Linux 7 x86_64 HVM EBS ENA 1901_01-b7ee8a69-ee97-4a49-9e68-afaee216db2e-ami-05713873c6794f575.4"
}

variable "instance_key_name" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "sg_egress_cidr_rules" {
  type = list(map(string))
}

variable "sg_ingress_cidr_rules" {
  type = list(map(string))
}

variable "vpc_public_subnets" {
  type = list(map(string))
}

variable "vpc_cidr" {
  type = string
}

variable "tags" {
  type = map(string)
}

