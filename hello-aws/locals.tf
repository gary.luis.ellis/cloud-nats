locals {
  # cidr based ingress and egress rules
  sg_cidr_rules = [
    { desc = "", from_port = "80", to_port = "80", protocol = "tcp", cidr_blocks = "0.0.0.0/0" },
    { desc = "", from_port = "443", to_port = "443", protocol = "tcp", cidr_blocks = "0.0.0.0/0" },
  ]

  # rules specific to the application that would be applied to any sdlc environment stage
  sg_ingress_cidr_rules = []

  sg_egress_cidr_rules = [
    { desc = "ntp egress", from_port = "123", to_port = "123", protocol = "tcp", cidr_blocks = "0.0.0.0/0" },
    { desc = "ntp egress", from_port = "123", to_port = "123", protocol = "udp", cidr_blocks = "0.0.0.0/0" },
  ]
}


# setup our instance bootstrap config
locals {
  cloud_init = <<EOF
---
#cloud-config
repo_update: true
repo_upgrade: all
package_upgrade: true

## create one partition on the ebs volume and mount it
bootcmd:
 - echo ";" | sfdisk /dev/xvdb
 - mkfs -t ext4 /dev/xvdb1
 - mkdir -p /data

mounts:
  - [ "/dev/xvdb1", "/data", "ext4", "defaults,nofail", "0", "2" ]
EOF


## deploy the application in its own script
  extra_user_data_script =<<EOF
#!/bin/bash

app_name="hello-aws"


docker run \
    -d \
    --restart unless-stopped \
    -p 80:80 \
    -v /data:/usr/share/nginx/html:ro \
    --name $app_name \
    nginx:1.16


echo "Hello AWS" > /data/index.html

# an extremely basic validation
cat > /data/volume.html <<EOT
#### lsblk -l
$(lsblk -l)

#### df -h
$(df -h)

#### cat /etc/fstab
$(cat /etc/fstab)
EOT
EOF
}
