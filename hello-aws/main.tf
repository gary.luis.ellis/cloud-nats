module "userdata" {
  source = "github.com/garyellis/tf_module_cloud_init"

  base64_encode          = false
  gzip                   = false
  install_docker         = true
  install_docker_compose = false
  extra_user_data_yaml   = local.cloud_init
  extra_user_data_script = local.extra_user_data_script
}


### our ec2 instance resource and eip
module "instance" {
  source = "github.com/garyellis/tf_module_aws_instance"
  count_instances             = var.instance_count
  ami_name                    = var.instance_ami_name
  associate_public_ip_address = true
  instance_type               = var.instance_type
  key_name                    = var.instance_key_name
  name                        = var.name
  security_group_attachments  = [module.sg.security_group_id]
  subnet_ids                  = module.vpc.public_subnets.*.id
  tags                        = var.tags
  user_data                   = module.userdata.cloudinit_userdata
  ebs_block_device = [{
    delete_on_termination = "false"
    device_name = "/dev/xvdb"
    volume_type = "gp2"
    volume_size = 1
  }]
}

resource "aws_eip" "instance_eip" {
  count = var.instance_count
  tags     = merge(var.tags, map("Name", var.name))
  vpc      = true
}

resource "aws_eip_association" "instance_eip" {
  count = var.instance_count
  instance_id   = element(module.instance.aws_instance_ids, count.index)
  allocation_id = lookup(element(aws_eip.instance_eip, count.index), "id")
}


### create our security group
module "sg" {
  source = "github.com/garyellis/tf_module_aws_security_group"

  description         = format("%s security group", var.name)
  egress_cidr_rules   = concat(local.sg_egress_cidr_rules, local.sg_cidr_rules, var.sg_egress_cidr_rules)
  ingress_cidr_rules  = concat(local.sg_ingress_cidr_rules, local.sg_cidr_rules, var.sg_ingress_cidr_rules)
  name                = var.name
  tags                = var.tags
  vpc_id              = module.vpc.vpc_id
}

### setup our mvp vpc
data "aws_availability_zones" "azs" {}

locals {
  azs = [data.aws_availability_zones.azs.names[0]]
}

module "vpc" {
  source = "github.com/garyellis/tf_module_aws_vpc"

  azs            = local.azs
  name           = var.name
  public_subnets = var.vpc_public_subnets
  vpc_cidr       = var.vpc_cidr
  tags           = var.tags
}
