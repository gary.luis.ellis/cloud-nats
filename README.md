# cloud-nats hello-aws
This repo is stores configuration for a demo hello aws environment. It creates the following configuration:

* A VPC with one public subnet
* A security group
* Security group ingress rules allowing http, https to 0.0.0.0/0 and ssh from a list of cidrs.
* Security group egress rules allowing http, https and ntp egress to 0.0.0.0/0 
* An EC2 instance with an EIP association and a asecondary ebs volume mounted on /data
* A multi-part cloud-init script that configures the secondary ebs volume mount to /data, installs docker and launches an nginx docker container
* Terraform state back end partial configuration to simplify creating multiple instances of the hello-aws module


## Requirements

* Terraform v0.12


## Usage

```bash
export AWS_REGION=us-east-2
# AWS access keys in environment are implied

export BUCKET_REGION=<bucket-region> BUCKET_NAME=ews-works BUCKET_KEY=hello-aws/demo/terraform-state

terraform init \
  -backend-config="region=$BUCKET_REGION" \
  -backend-config="bucket=$BUCKET_NAME" \
  -backend-config="key=$BUCKET_KEY" \
  hello-aws

terraform apply --var-file terraform.tfvars hello-aws
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | A common name applied to taggable resources | `string` | n/a | yes |
| instance\_count | The number of ec2 instances to deploy | `number` | `1` | no |
| instance\_key\_name | An existing aws  keypair | `string` | `""` | no |
| instance\_type | The ec2 instance type | `string` | `t2.nano` | yes |
| instance\_ami\_name | The ec2 instance AMI | `string` | `CentOS Linux 7 x86_64 HVM EBS ENA 1901_01-b7ee8a69-ee97-4a49-9e68-afaee216db2e-ami-05713873c6794f575.4` | yes |
| sg\_egress\_cidr\_rules | A list of egress cidr rule maps | `list(map(string))` | `[]` | no |
| sg\_ingress\_cidr\_rules | A list of ingress cidr rule maps | `list(map(string))` | `[]` | no |
| vpc\_public\_subnets | A list of subnet maps | `list(map(string))` | n/a | yes |
| vpc\_cidr | The VPC CIDR block | `string` | n/a | yes |
| tags | A map of tags applied to taggable resources | `map(string)` | `{}` | no |

## Outputs

| Name | Description | Type |
|------|-------------|:------:|
| aws\_instance\_eips | A list of EIPs associated associated to instances | `list(string)` |
| curl | a newline delimited list of curl commands targeting the aws instance eips | `string` |
