name = "demo"
instance_count = 1
instance_key_name = "garyellis"
instance_type = "t2.nano"
sg_ingress_cidr_rules = [
  { desc = "admin ssh", from_port = "22", to_port = "22", protocol = "tcp", cidr_blocks = "72.194.238.100/32,70.190.137.162/32" }
]
sg_egress_cidr_rules = []
vpc_cidr = "10.240.0.0/22"
vpc_public_subnets = [
  { name = "public", cidr = "10.240.1.128/26" },
]
tags = {
  environment_stage = "foo"
  owner = "garyellis"
}
